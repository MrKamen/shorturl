package dev.mrkamen.shorturlproject.repository;

import dev.mrkamen.shorturlproject.entity.Link;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LinkRepository extends JpaRepository<Link, Long> {

    @Query(value = "SELECT * FROM links WHERE shorted_link = ?1", nativeQuery = true)
    Link getLink(String shortURL);

    Link getByshortedLink(String shortedLink);

}
