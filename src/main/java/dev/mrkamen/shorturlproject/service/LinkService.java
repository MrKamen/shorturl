package dev.mrkamen.shorturlproject.service;

import dev.mrkamen.shorturlproject.entity.Link;
import dev.mrkamen.shorturlproject.repository.LinkRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@Log4j2
@Service
@AllArgsConstructor
public class LinkService {

    private static String domain = "https://dev.agemagic.ru";
//    private static String domain = "http://localhost";

    private LinkRepository repository;


    public String shortLink(Link unShortedLink) {
        int length = 8;
        String alphaNumericString = "ABCDEFGHIJKLMNPQRSTUVWXYZ"
                + "123456789"
                + "abcdefghijklmnpqrstuvxyz";
        byte[] bytes = unShortedLink.getUnShortedLink().getBytes();
        StringBuilder sb = new StringBuilder();

        bytes = shuffleArray(bytes);

        for (int i = 0; i < length; i++) {

            sb.append(alphaNumericString.charAt(bytes[i % bytes.length] % (alphaNumericString.length() - 1)));
        }


        unShortedLink.setShortedLink(sb.toString());

        repository.saveAndFlush(unShortedLink);

        return sb.toString();
    }


    public String unShortLink(String shortLink) {
        String response = domain;
        Date nowDate = new Date();

        Link temporaryLink = repository.getLink(shortLink);

        if (temporaryLink == null) {
            return response;
        }

        if (temporaryLink.getStartDate() != null && temporaryLink.getStartDate().after(nowDate)) {
            return response;
        }

        if (temporaryLink.getEndDate() != null && temporaryLink.getEndDate().before(nowDate)) {
            repository.delete(temporaryLink);
            return response;
        }

        return temporaryLink.getUnShortedLink();
    }


    private String getAlphaNumericString(int n) {

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }


    static byte[] shuffleArray(byte[] ar) {
        // If running on Java 6 or older, use `new Random()` on RHS here
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            byte a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
        return ar;

    }

}
