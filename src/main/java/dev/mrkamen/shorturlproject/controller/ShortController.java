package dev.mrkamen.shorturlproject.controller;

import dev.mrkamen.shorturlproject.entity.Link;
import dev.mrkamen.shorturlproject.service.LinkService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.Map;

@RestController
@AllArgsConstructor
@Log4j2
public class ShortController {
    private final LinkService service;

    @PostMapping(path = "/shortMyLink")
    public Map<String, String> shortLink(@RequestBody Link link) {
        HashMap<String, String> map = new HashMap<>();
        String unShortedLink = link.getUnShortedLink();

        boolean validateUrl = unShortedLink.matches(
                "(https?:\\/\\/)([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w\\.-]*)*\\/?");
        map.put("ValidateUrl", String.valueOf(validateUrl));

        if (!validateUrl) {
            map.put("unShortLink", "none");
            map.put("shortLink", "none");

            return map;
        }

        map.put("unShortLink", unShortedLink);
        map.put("shortLink", service.shortLink(link));

        return map;
    }

    @GetMapping(path = "/{shortLink}")
    public RedirectView unShortLink(@PathVariable String shortLink) {
        return new RedirectView(service.unShortLink(shortLink));
    }

}


