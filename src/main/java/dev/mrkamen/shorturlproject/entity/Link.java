package dev.mrkamen.shorturlproject.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "links")
@NoArgsConstructor
public class Link {

    public Link(String unShortedLink, String shortedLink) {
        this.shortedLink = shortedLink;
        this.unShortedLink = unShortedLink;
    }

    public Link(String unShortedLink){
        this.unShortedLink = unShortedLink;
    }

    @Id
    @GeneratedValue
    private Long id;
    @Version
    private Long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSerialVersionUID() {
        return serialVersionUID;
    }

    public void setSerialVersionUID(Long serialVersionUID) {
        this.serialVersionUID = serialVersionUID;
    }

    public String getUnShortedLink() {
        return unShortedLink;
    }

    public void setUnShortedLink(String unShortedLink) {
        this.unShortedLink = unShortedLink;
    }

    public String getShortedLink() {
        return shortedLink;
    }

    public void setShortedLink(String shortedLink) {
        this.shortedLink = shortedLink;
    }

    @URL
    private String unShortedLink;

    private String shortedLink;

    private Date startDate;
    private Date endDate;

}
