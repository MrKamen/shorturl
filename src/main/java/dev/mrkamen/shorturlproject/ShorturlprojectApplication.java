package dev.mrkamen.shorturlproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShorturlprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShorturlprojectApplication.class, args);
    }

}
